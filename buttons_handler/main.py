from multiprocessing import *
from socketIO_client import SocketIO

import os
import time
import RPi.GPIO as GPIO
import json

GPIO.setmode(GPIO.BCM)

# SocketIO talks to NodeJS client side
socketIO = SocketIO('localhost', 8999)

#Button BCM mappings
main_btn_press = 12
main_btn_spinA = 6
main_btn_spinB = 13

GPIO.setup(main_btn_press, GPIO.IN)
GPIO.setup(main_btn_spinA, GPIO.IN)
GPIO.setup(main_btn_spinB, GPIO.IN)

#Events
btn_clicked_Event = Event()
btn_spin_left_Event = Event()
btn_spin_right_Event = Event()
go_to_sleep_Event = Event()
speaking_Event = Event()

btn_press_interval_max_delay = 0.5
safety_delay = 5
next_device_delay = 2.5
long_press_duration = 3
no_activity_sleep_delay = 20
button_press_counter = 0.3
spin_lock_release_delay = 0.1

try_to_use_real_devices = True

example_active_devices = [["device" + str(i), i] for i in range(0, 10)]
devices_json_response = ""


def on_device_response(devices_json):
    # Response we actually expect is [["device1Name", 1], ["device2Name", 2]]
    # Numbers come as ID of device, it could be any unique number actually.
    print("Got following json response: " + str(devices_json))
    global devices_json_response
    devices_json_response = str(devices_json)

# What we will do if we will get devices.
socketIO.on('devicesReplyPython', on_device_response)

# Try to get active devices list
socketIO.emit('connection')
socketIO.emit('getDevicesPython')
print("Waiting 5 sec for devices list")
time.sleep(safety_delay)


def speak(phrase, speaking_event):
    speaking_event.set()
    os.system('echo "' + phrase + '" | festival --tts')
    speaking_event.clear()


def button_is_pressed(button_pressed_state):
    if button_pressed_state == GPIO.input(main_btn_press):
        time.sleep(button_press_counter)
        return True
    return False


# NB! This event is not clearing itself.
def button_long_press_handler(go_to_sleep_event, button_clicked_event):
    btn_default_state = GPIO.input(main_btn_press)
    btn_pressed_state = not btn_default_state
    while True:
        if btn_default_state != GPIO.input(main_btn_press):
            total_time = 0
            while button_is_pressed(btn_pressed_state):
                total_time += button_press_counter
                if total_time >= long_press_duration:
                    go_to_sleep_event.set()
                    print("Long press happens")
                    break
            if total_time < long_press_duration:
                print("Clicked")
                button_clicked_event.set()


def start_timer(time_to_sleep, go_to_sleep_event):
    while time_to_sleep.value > 0:
        time.sleep(1)
        time_to_sleep.value -= 1
        if time_to_sleep.value % 5 == 0:
            print("Time to autosleep: " + str(time_to_sleep.value) + " seconds")
    print("Inactivity timer : " + str(no_activity_sleep_delay) + " are left, going to IDLE")
    go_to_sleep_event.set()


def wait_for_click(button_clicked_event, time_to_sleep):
    while True:
        button_clicked_event.wait()
        time_to_sleep.value = no_activity_sleep_delay


def wait_left_spin(button_spin_left_event, time_to_sleep):
    while True:
        button_spin_left_event.wait()
        time_to_sleep.value = no_activity_sleep_delay


def wait_right_spin(button_spin_right_event, time_to_sleep):
    while True:
        button_spin_right_event.wait()
        time_to_sleep.value = no_activity_sleep_delay


def inactivity_sleep_handler(go_to_sleep_event, button_clicked_event, button_spin_left_event, button_spin_right_event):
    time_to_sleep = Value('i', no_activity_sleep_delay)
    Process(target=start_timer, args=[time_to_sleep, go_to_sleep_event]).start()
    Process(target=wait_for_click, args=[button_clicked_event, time_to_sleep]).start()
    Process(target=wait_left_spin, args=[button_spin_left_event, time_to_sleep]).start()
    Process(target=wait_right_spin, args=[button_spin_right_event, time_to_sleep]).start()


def release_lock(lock):
    time.sleep(spin_lock_release_delay)
    lock.value = 0


def am_i_first(lock):
    if lock.value == 0:
        lock.value = 1
        Process(target=release_lock, args=[lock]).start()
        return True
    elif lock.value == 1:
        return False


# NB! These events is not clearing itself.
def button_spin_handler(button_spin_left_event, button_spin_right_event):
    start_state_a = GPIO.input(main_btn_spinA)  # FOR 6
    start_state_b = GPIO.input(main_btn_spinB)  # FOR 13
    # 0 for unset lock, 1 for set lock
    first_lock = Value('i', 0)
    while True:
        if start_state_a != GPIO.input(main_btn_spinA):
            if am_i_first(first_lock):
                button_spin_left_event.set()
            start_state_a = GPIO.input(main_btn_spinA)

        if start_state_b != GPIO.input(main_btn_spinB):
            if am_i_first(first_lock):
                button_spin_right_event.set()
            start_state_b = GPIO.input(main_btn_spinB)


def button_click_count_handler(button_clicked_event, button_click_count_event, times_button_clicked):
    while True:
        button_clicked_event.wait()
        times_button_clicked.value = 1
        button_clicked_event.clear()
        for i in range(2):
            button_clicked_event.wait(btn_press_interval_max_delay)
            if button_clicked_event.is_set():
                times_button_clicked.value += 1
                button_clicked_event.clear()

        # Only triggering others
        button_click_count_event.set()
        button_click_count_event.clear()


def read_device_name(device_name, speaking_event):
    speak(device_name, speaking_event)
    time.sleep(next_device_delay)


def read_devices_inParallel(active_devices, index, go_to_sleep_event, speaking_event):
    list_is_empty = (len(active_devices) == 0)
    if list_is_empty:
        speak("There is no active devices", speaking_event)
        go_to_sleep_event.set()
        print("No devices activated")
    else:
        # For starting on right item
        index.value = -1
        while not list_is_empty:
            while not speaking_event.is_set():
                if len(active_devices) == 0:
                    read_devices_inParallel(active_devices, index, go_to_sleep_event, speaking_event)
                    list_is_empty = True
                    break
                index.value += 1
                if index.value >= len(active_devices):
                    index.value %= len(active_devices)
                if index.value < 0:
                    index.value += len(active_devices)
                Process(target=read_device_name, args=[active_devices[index.value][0], speaking_event]).run()


def turn_off_device(active_devices, id):
    device = active_devices.pop(id)
    socketIO.emit("turnOffPython", device[1])
    print("Turning off device " + str(device))


def turn_all_devices_off(active_devices):
    print("Removing all active devices")
    while len(active_devices) > 0:
        turn_off_device(active_devices, 0)
        time.sleep(0.20)


def handle_multiple_clicks_inParallel(button_clicked_event, index, speaking_event, active_devices):
    button_press_count_event = Event()
    times_button_clicked = Value('i', 0)
    btn_multiclick_handle = Process(target=button_click_count_handler, args=[button_clicked_event, button_press_count_event, times_button_clicked])
    btn_multiclick_handle.start()
    while True:
        times_button_clicked.value = 0
        button_press_count_event.wait()
        print("Main button pressed " + str(times_button_clicked.value) + " times")
        if times_button_clicked.value == 1:
            print("One click, going back in list")
            index.value -= 2
        elif times_button_clicked.value == 2:
            speak("Turning this device" + str(index.value) + " off", speaking_event)
            Process(target=turn_off_device, args=[active_devices, index.value]).start()
            print("Turning current device off")
        elif times_button_clicked.value == 3:
            speak("Turning all active devices off", speaking_event)
            Process(target=turn_all_devices_off, args=[active_devices]).start()
            print("Turning all active devices off")


def handle_spin_inParallel(button_left_spin_event, button_right_spin_event, index):
    while True:
        if button_left_spin_event.is_set():
            print("Left spin")
            index.value -= 1
            button_left_spin_event.clear()
        elif button_right_spin_event.is_set():
            index.value += 1
            print("Right spin")
            button_right_spin_event.clear()


def main(button_clicked_event, button_left_spin_event, button_right_spin_event, go_to_sleep_event, speaking_event):
    while True:
        button_clicked_event.wait()
        print("User clicked one time, exiting sleep mode")
        button_clicked_event.clear()

        print("Getting list of active devices")
        manager = Manager()

        if try_to_use_real_devices:
            active_devices = manager.list(json.loads(devices_json_response))
            print("Active devices list running with real devices: " + str(active_devices))
        else:
            active_devices = manager.list(example_active_devices)

        index = Value('i', 0)

        handling_processes = list()
        handling_processes.append(Process(target=read_devices_inParallel, args=[active_devices, index, go_to_sleep_event, speaking_event]))
        handling_processes.append(Process(target=handle_multiple_clicks_inParallel, args=[button_clicked_event, index, speaking_event, active_devices]))
        handling_processes.append(Process(target=handle_spin_inParallel, args=[button_left_spin_event, button_right_spin_event, index]))
        handling_processes.append(Process(target=inactivity_sleep_handler, args=[go_to_sleep_event, button_clicked_event, button_left_spin_event, button_right_spin_event]))

        for h_process in handling_processes:
            h_process.start()

        go_to_sleep_event.wait()
        go_to_sleep_event.clear()
        print("Terminate all handlers and readers")
        for h_process in handling_processes:
            h_process.terminate()
        print("Deleting processes")
        for h_process in handling_processes:
            del h_process
        print("Entering IDLE")
        speak("Going to sleep mode", speaking_event)
        time.sleep(safety_delay)

# Main entry point
if __name__ == '__main__':
    time_until_sleep = Value('i', no_activity_sleep_delay)
    processes = list()
    p_btn_press_handle = Process(target=button_long_press_handler, args=[go_to_sleep_Event, btn_clicked_Event])
    p_btn_spin_handle = Process(target=button_spin_handler, args=[btn_spin_left_Event, btn_spin_right_Event])
    p_main = Process(target=main, args=[btn_clicked_Event, btn_spin_left_Event, btn_spin_right_Event, go_to_sleep_Event, speaking_Event])

    processes.append(p_btn_press_handle)
    processes.append(p_btn_spin_handle)
    processes.append(p_main)

    for process in processes:
        process.start()
    for process in processes:
        process.join()
