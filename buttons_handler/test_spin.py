import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
delay_after_press = 0.3

test_button1 = 13
test_button2 = 6

GPIO.setup(test_button1, GPIO.IN)
GPIO.setup(test_button2, GPIO.IN)

press_counter = 0

test_button1_d = GPIO.input(test_button1)
test_button2_d = GPIO.input(test_button2)
iteration = 0
while True:
  if (test_button1_d != GPIO.input(test_button1)):
    print("13 new value: " + str(GPIO.input(test_button1)))
    test_button1_d = GPIO.input(test_button1)
  if (test_button2_d != GPIO.input(test_button2)):
    print("6 new value: " + str(GPIO.input(test_button2)))
    test_button2_d = GPIO.input(test_button2)
  time.sleep(0.01)
