function processData(jsonAnswer){
    $('#wrapper').empty();
    $('#wrapper').append
    ('<table class="table table-bordered" style="width: 80%; margin: 0 auto;">'
    + '<thead><tr>'
    +   '<th>WiFi Name</th><th>Password Field</th><th>Push to Connect</th>'
    + '</tr></thead>'
    + '<tbody id="wifi_table"></tbody>'
    +'</table>');
    for(i = 0; i < jsonAnswer.length; i++){
        var passwordField = '<td><center style="font-size: 19px">No Password Needed</center></td>';
        if(jsonAnswer[i][2]=='protected'){
            passwordField = '<td><input type="password" class="form-control" id="pwd"></td>';
        }
        $('#wifi_table').append
        (
            '<tr><td><center style="font-size: 19px">'+jsonAnswer[i][1]+'</center></td>' +
             passwordField +
            '<td><button type="button" class="btn btn-success btn-block" style="font-size: 20px">Connect</button></td></tr>'
        );
    }
}

$.ajax({
    type: "GET",
    url: "http://localhost:8081"
}).done(function (data) {
    data = JSON.parse(data);
    processData(data);
});
