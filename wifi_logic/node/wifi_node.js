var http = require('http');
var cmd = require('node-cmd');
var qs = require('querystring');
http.createServer(function(request, response) {
        response.writeHeader(200, {"Content-Type": "text/html", "Access-Control-Allow-Origin": "*", "Access-Control-Allow-Headers": "Content-Type"});
		if(request.url=="/"){
			console.log("Scanning wifi");
			cmd.get('sudo wifi scan',
				function(data){
					data = String(data).trim();
					data = data.split("\n");
					for(i = 0; i < data.length; i ++){
							data[i] = data[i].split(/\s+/);
				}
				response.end(JSON.stringify(data));
			});
		}else if(request.url='/connectToWifiWithPassword'){
			request.on('data', function(data){
				console.log("with pw");
				data = String(data);
				data = data.split('&');
				essid = data[0].split('=')[1];
				password = data[1].split('=')[1];
				console.log(essid +":" + password);
				cmd.get("sudo expect connect " + essid + " " +password,
					function(data){response.end(data);});
				});
		}else if(request.url='/connectToWifiWithoutPassword'){
			request.on('data', function(data){
				console.log("without pw");
				data = String(data);
				data = data.split('=');
				essid = data[1];
				console.log(essid);	
				cmd.get("sudo expect instaconnect " + essid, function(data){
					response.end(data);
				});

		});
		}

}).listen(8081);
