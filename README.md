Button handler
==============
For old file:
Working example of how we can call JSONRPC requests from python script handling buttons input.
For new file:
Multiprocess application, that handles all button spin/push events properly according to documentation.

Node applications
=========
To get PI and website working you first have to make some changes in database. Change website MAC to the MAC where the website runs(probably your computer) and mati's mac to Raspberry PI's MAC.
All the applications should start working when running two commands in each folder:
- npm install
- npm start

Client
======
Client is running in Raspberry PI, communicating with server via port 8001, and making JSONRPC calls on servers request.

server
======
Server is communicating with PI, sqlite database and website via port 8001. When device connects, the server waits 1 second for device MAC address and when it wasn't sent or can't find sent MAC from database it disconnects the client. When the MAC address is OK, then it looks which client has device with this MAC address and adds its socketid to database(when the device disconnects, the socketid is removed). When user makes request from website, we also get username with the request and from database we get his/her device socketid and send request to it.

website
=======
Website runs on its own node application(PORT 3000), which also is working as socket.io server and client. User is sending requests to website socket.io server and it forwards them to the main server, redirects responses to user. At the moment, it's working with one user called mati, whos name is used in all requests.

Wifi controller
=======
For wifi controller, we have Node.js application and website under apache. One is working in REST Api role, other one is website that is accessible on 172.24.1.1 address when you are connected to PI.
This controller uses python [wifi module](https://wifi.readthedocs.io/en/latest/) for searching and connecting to networks. 

Content of "wifiweb" folder, should be placed to apache "www" folder. Apache should be restarted afterwards.
For running Node.js app, it's needed to run "node wifi_node.js" command in "wifi_logic/node" directory.
Both should work, in other case - it wouldn't be possible to make RPi connect to any WiFi.

update
=======
Since authentication running mongodb is required for storing users and sessions.