var socket = io.connect('');


setInterval(function() {
    getDevices();
}, 5000);
var moveFocus = function(serial) {
    var current_switch_id, inputs, current_index, next_index, next_index_limit, next_switch_id, i;
    current_switch_id = "switch_" + serial;
    inputs = gatherSwitches();
    next_index_limit = inputs.length - 1;
    current_index = 0;
    if (inputs.length === 0) {
        return false;
    }
    for (i = 0; i < inputs.length; i++) {
        var current_input_id,
            current_input_id = inputs[i].id;
        if (current_input_id === current_switch_id) {
            current_index = i;
            break;
        }
    }
    next_index = current_index + 1;
    if (next_index === inputs.length) {
        next_index = 0;
    }
    next_switch_id = inputs[next_index].id;
    setTimeout(function() {
        document.getElementById(next_switch_id).focus();
    }, 250);
}
var gatherSwitches = function() {
    return document.getElementsByClassName("switch-input");
}

function getDevices() {
    socket.emit('getDevices');
}

function turnOff(id) {
    socket.emit('turnOff', id);
    getDevices();
    moveFocus(id);
}

function turnOn(id) {
    socket.emit('turnOn', id);
    getDevices();
    moveFocus(id);
}

socket.on('devicesReply', function(data) {
    var a = data,
        i;
    var keys = Object.keys(a);
    $('#devices_list').empty();
    for (i = 0; i < keys.length; i++) {
        var device = a[keys[i]]['data']['givenName']['value'];
        var status = a[keys[i]]['instances']['0']['commandClasses']['32']['data']['level']['value'];
        var id = a[keys[i]]['id'];
        if (device != null && status != null) {
            if (activeDevices && status == '255') {
                $('#devices_list').append('<li><span id="device_name">' + device + '</span><span id="device_switch"><label class="switch switch-green"><input aria-label="' + device + 'is turned on" type="checkbox" id="switch_' + id + '" class="switch-input" checked onclick = "turnOff(' + id + ');"><span aria-hidden="true" class="switch-label" data-on="On" data-off="Off"></span><span class="switch-handle"></span></label><span></li>');
            } else if (!activeDevices && status == '0') {
                $('#devices_list').append('<li><span id="device_name">' + device + '</span><span id="device_switch"><label class="switch switch-green"><input aria-label="' + device + 'is turned off" type="checkbox" id="switch_' + id + '" class="switch-input" unchecked onclick = "turnOn(' + id + ');"><span aria-hidden="true" class="switch-label" data-on="On" data-off="Off"></span><span class="switch-handle"></span></label><span></li>');
            }
        }
    }
    if ($('#devices_list li').length == 0) {
        $('#devices_list').append('<li>No devices in this view.</li>');
    }
});

function find_previous_next_users(num, isCalledFromPaginator) {
    console.log(num);
    if (isCalledFromPaginator === true) {
        $("#message").empty();
    }
    var xmlhttp;
    xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
            if (xmlhttp.status == 200) {
                var result, rows;
                $("#users_b").empty();
                result = $.parseJSON(xmlhttp.responseText);
                console.log(result);
                var users = result.users;
                var req_user = result.user;
                var size = result.size;
                var totalRecords = result.totalRecords;
                for (var i = 0; i < users.length; i++) {
                    var user, row;
                    user = users[i];
                    console.log(user);
                    if (req_user._id === user._id) {
                        row = '<tr><td>' + user._id + '</td><td>' + user.username + '</td><td>' + user.firstName + '</td><td>' + user.lastName + '</td><td>' + user.email + '</td><td>' + user.deviceMac + '</td><td>' + user.isAdmin + '</td><td><button onclick="editRow(' + user._id + ')">Edit</button></td><td id="save_' + user._id + '"><form method="post" action="/users" id="users_data_' + user._id + '"><input type="hidden" id="user_id_' + user._id + '" name="user_id" value="' + user._id + '" disabled><input id="save_button_' + user._id + '" type="submit" value="Save" disabled></form></td></tr>';
                    } else {
                        row = '<tr><td>' + user._id + '</td><td>' + user.username + '</td><td>' + user.firstName + '</td><td>' + user.lastName + '</td><td>' + user.email + '</td><td>' + user.deviceMac + '</td><td>' + user.isAdmin + '</td><td><button onclick="editRow(' + user._id + ')">Edit</button></td><td id="save_' + user._id + '"><form method="post" action="/users" id="users_data_' + user._id + '"><input type="hidden" id="user_id_' + user._id + '" name="user_id" value="' + user._id + '" disabled><input id="save_button_' + user._id + '" type="submit" value="Save" disabled></form></td><td id="delete_' + user._id + '"><button onclick="deleteFunction(\'' + user._id + '\')">Delete</button></td></tr>';
                    }
                    rows += row;
                }
                $("#users_b").html(rows);
            }
        }
    }
    xmlhttp.open("GET", "/users?page=" + num + "&result=next", true);
    xmlhttp.send();
}

function editRow(user_id) {
    var td_tags = ['uname', 'fname', 'lname', 'email', 'dmac', 'isAdmin'];
    var input_ids = ['input_uname', 'input_fname', 'input_lname', 'input_email', 'input_dmac', 'input_isAdmin'];
    var input_names = ['uname_input', 'fname_input', 'lname_input', 'email_input', 'dmac_input', 'isAdmin_input'];
    var n = td_tags.length;
    var i = 0;
    for (i; i < n; i++) {
        var cell = $("#" + td_tags[i] + "_" + user_id);
        var data_chunk = cell.html();
        if (typeof data_chunk != 'undefined') {
            data_chunk = data_chunk.trim();
        }
        var type = 'text';
        if (td_tags[i] == 'email') {
            type = 'email';
        }
        var input = "<input type='" + type + "' id='" + input_ids[i] + "_" + user_id + "' name='" + input_names[i] + "' value='" + data_chunk + "' form='users_data_" + user_id + "'/>";
        cell.html(input);
    }
    $("#save_button_" + user_id).prop('disabled', false);
}

function deleteFunction(user_id) {
    var last_name = clarify_last_name();
    deleteUser(user_id, last_name);
}

function clarify_last_name() {
    var last_name = prompt("Enter user´s lastname:", "");
    return last_name;
}

function deleteUser(user_id, last_name) {
    $("#message").empty();
    var xmlhttp;
    xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
            if (xmlhttp.status == 200) {
                var result;
                $("#message").empty();
                result = $.parseJSON(xmlhttp.responseText);
                if (result.isRemoved === true) {
                    $("#message").html("User is removed.");
                } else {
                    $("#message").html("You entered wrong lastname. User is not removed.");
                }
            }
        }
    }
    xmlhttp.open("GET", "/delete/" + user_id + "?name=" + last_name, true);
    xmlhttp.send();
    location.reload();
}
