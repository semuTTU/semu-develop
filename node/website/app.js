var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var dbConfig = require('./db');
var mongoose = require('mongoose');
var ios = require('socket.io-express-session');
var mac = require('getmac');
var socket_io = require('socket.io');
var io = socket_io();
var routes = require('./routes/index');
var app = express();
var passport = require('passport');
var initPassport = require('./passport/init');
var expressSession = require('express-session');

var nodemailer = require('nodemailer');
var async = require('async');
var crypto = require('crypto');

var flash = require('connect-flash');
var MongoStore = require('connect-mongo')(expressSession);
var session = expressSession({
  store: new MongoStore({
    url: 'mongodb://localhost/semu-sessions'
  }),
  secret: 'mySecretKey',
  resave: false,
  saveUninitialized: false
})
mongoose.connect(dbConfig.url);

var scribe = require('scribe-js')();
var console = process.console;
// app.use('/logs', scribe.webPanel());
app.use(scribe.express.logger());
app.use(session);

io.use(ios(session));
app.io = io;
var routes = require('./routes/socket')(io);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(passport.initialize());
app.use(passport.session());


app.use(flash());

initPassport(passport);

var routes = require('./routes/index')(passport, scribe, async, crypto, nodemailer);

app.use('/', routes);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;
