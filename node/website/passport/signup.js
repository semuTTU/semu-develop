
var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');
var validator = require('validator');
var bCrypt = require('bcrypt-nodejs');

module.exports = function(passport) {

    passport.use('signup', new LocalStrategy({
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done) {

            findOrCreateUser = function() {
                // find a user in Mongo with provided username
                User.findOne({
                    'username': username
                }, function(err, user) {
                    // In case of any error, return using the done method
                    if (err) {
                        console.error('Error in SignUp: ' + err);
                        return done(err);
                    }
                    // already exists
                    if (user) {
                        console.error('User already exists with username: ' + username);
                        return done(null, false, req.flash('errorMessage', 'User Already Exists with that username'));
                    } else {
                        User.findOne({
                            'email': req.param('email')
                        }, function(err, email) {
                            if (err) {
                                console.error('Error in SignUp: ' + err);
                                return done(err);
                            }
                            if (email) {
                                console.error('User already exists with email: ' + req.param('email'));
                                return done(null, false, req.flash('errorMessage', 'User Already Exists with that email'));
                            } else {
			    	var mac = req.param('deviceMac');
                                if (!req.param('admin')) {
                                    if (!mac) {
                                        return done(null, false, req.flash('errorMessage', 'MAC address not specified!'));
                                    }
                                    User.findOne({
                                        'deviceMac': mac
                                    }, function(err, deviceMac) {
                                        if (err) {
                                            console.error('Error in SignUp: ' + err);
                                            return done(err);
                                        }
                                        if (deviceMac) {
                                            console.error('User already exists with that MAC: ' + mac);
                                            return done(null, false, req.flash('errorMessage', 'User Already Exists with that MAC'));
                                        } else {
                                            if (!validator.isMACAddress(mac)) {
                                                console.error('MAC address not valid!');
                                                return done(null, false, req.flash('errorMessage', 'MAC address not valid!'));
                                            } else {

                                                // if there is no user with that email
                                                // create the user
                                                var newUser = new User();

                                                // set the user's local credentials
                                                newUser.username = username;
                                                newUser.password = createHash(password);
                                                newUser.email = req.param('email');
                                                newUser.firstName = req.param('firstName');
                                                newUser.lastName = req.param('lastName');
                                                newUser.deviceMac = mac;
                                                newUser.isAdmin = req.param('admin');

                                                // save the user
                                                newUser.save(function(err) {
                                                    if (err) {
                                                        console.log('Error in Saving user: ' + err);
                                                        throw err;
                                                    }
                                                    console.log('User Registration succesful');
                                                    return done(null, false, req.flash('successMessage', 'User registered'));
                                                });

                                            }

                                        }
                                    });
                                }else{
                                    var newUser = new User();

                                                // set the user's local credentials
                                                newUser.username = username;
                                                newUser.password = createHash(password);
                                                newUser.email = req.param('email');
                                                newUser.firstName = req.param('firstName');
                                                newUser.lastName = req.param('lastName');
                                                newUser.deviceMac = mac;
                                                newUser.isAdmin = req.param('admin');

                                                // save the user
                                                newUser.save(function(err) {
                                                    if (err) {
                                                        console.log('Error in Saving user: ' + err);
                                                        throw err;
                                                    }
                                                    console.log('User Registration succesful');
                                                    return done(null, false, req.flash('successMessage', 'User registered'));
                                                });
                                }
                            }
                        });

                    }
                });
            };
            // Delay the execution of findOrCreateUser and execute the method
            // in the next tick of the event loop
            process.nextTick(findOrCreateUser);
        }));

    // Generates hash using bCrypt
    var createHash = function(password) {
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    }

}
