var express = require('express');
var router = express.Router();
var User = require('../models/user');
var bcrypt = require('bcrypt-nodejs');
var mongoose = require('mongoose');


var isAuthenticated = function(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
}
var isAdmin = function(req, res, next) {
    if (req.isAuthenticated()) {
        if (req.user.isAdmin) {
            return next();
        }
    }
    res.redirect('/');

}
module.exports = function(passport, scribe, async, crypto, nodemailer) {
    router.post('/reset/:token', function(req, res) {
        if (req.body.password != req.body.confirm) {
            req.flash('message', 'Passwords do not match!');
            res.redirect('back');
        } else {
            async.waterfall([
                function(done) {
                    User.findOne({
                        resetPasswordToken: req.params.token,
                        resetPasswordExpires: {
                            $gt: Date.now()
                        }
                    }, function(err, user) {
                        if (!user) {
                            req.flash('error', 'Password reset token is invalid or has expired.');
                            return res.redirect('back');
                        }
                        console.log(user.password);
                        user.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10), null);
                        user.resetPasswordToken = undefined;
                        user.resetPasswordExpires = undefined;

                        user.save(function(err) {
                            req.logIn(user, function(err) {
                                done(err, user);
                            });
                        });
                    });
                },
                function(user, done) {
                    var transporter = nodemailer.createTransport('smtps://resetsempw@gmail.com:Parool123@smtp.gmail.com');
                    var mailOptions = {
                        to: user.email,
                        from: 'resetsempw@gmail.com',
                        subject: 'Your password has been changed',
                        text: 'Hello,\n\n' +
                            'This is a confirmation that the password for your account ' + user.email + ' has just been changed.\n'
                    };
                    transporter.sendMail(mailOptions, function(err) {
                        req.flash('success', 'Success! Your password has been changed.');
                        done(err);
                    });
                }
            ], function(err) {
                res.redirect('/');
            });
        }
    });
    router.get('/reset/:token', function(req, res) {
        User.findOne({
            resetPasswordToken: req.params.token,
            resetPasswordExpires: {
                $gt: Date.now()
            }
        }, function(err, user) {
            if (!user) {
                req.flash('error', 'Password reset token is invalid or has expired.');
                return res.redirect('/forgot');
            }
            res.render('reset', {
                user: req.user,
                message: req.flash('message')
            });
        });
    });
    router.post('/forgot', function(req, res, next) {
        async.waterfall([
            function(done) {
                crypto.randomBytes(20, function(err, buf) {
                    var token = buf.toString('hex');
                    done(err, token);
                });
            },
            function(token, done) {
                User.findOne({
                    email: req.body.email
                }, function(err, user) {
                    if (!user) {
                        req.flash('error', 'No account with that email address exists.');
                        return res.redirect('/forgot');
                    }

                    user.resetPasswordToken = token;
                    user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

                    user.save(function(err) {
                        done(err, token, user);
                    });
                });
            },
            function(token, user, done) {
                var transporter = nodemailer.createTransport('smtps://resetsempw@gmail.com:Parool123@smtp.gmail.com');
                var mailOptions = {
                    to: user.email,
                    from: 'resetsempw@gmail.com',
                    subject: 'Node.js Password Reset',
                    text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                        'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
                        'http://' + req.headers.host + '/reset/' + token + '\n\n' +
                        'If you did not request this, please ignore this email and your password will remain unchanged.\n'
                };
                transporter.sendMail(mailOptions, function(err, info) {
                    req.flash('info', 'An e-mail has been sent to ' + user.email + ' with further instructions.');
                    done(err, 'done');
                });
            }
        ], function(err) {
            if (err) return next(err);
            res.redirect('/forgot');
        });
    });

    router.get('/forgot', function(req, res) {
        res.render('forgot', {
            user: req.user,
            error: req.flash('error'),
            info: req.flash('info'),
        });
    });
    router.use('/logs', isAdmin, scribe.webPanel());
    router.get('/', function(req, res) {
        if (req.user) {
            res.redirect('/active');
        } else {
            res.render('index', {
                message: req.flash('message')
            });
        }

    });

    /* Handle Login POST */
    router.post('/login', passport.authenticate('login', {
        successRedirect: '/active',
        failureRedirect: '/',
        failureFlash: true
    }));

    /* GET Registration Page */
    router.get('/signup', isAdmin, function(req, res) {
        res.render('register', {
            title: 'register',
            user: req.user,
            errorMessage: req.flash('errorMessage'),
            successMessage: req.flash('successMessage')
        });

    });


    /* Handle Registration POST */
    router.post('/signup', isAdmin, passport.authenticate('signup', {
        failureRedirect: '/signup',
        failureFlash: true
    }));



    /* GET Home Page */
    router.get('/active', isAuthenticated, function(req, res) {
        res.render('active', {
            user: req.user
        });
    });
    router.get('/inactive', isAuthenticated, function(req, res) {
        res.render('inactive', {
            user: req.user
        });
    });
    router.post('/profile', isAuthenticated, function(req, res) {
        if (req.body.password != req.body.confirm) {
            req.flash('error', 'Passwords do not match!');
            res.redirect('back');
        } else {
            User.findOne({
                username: req.user.username
            }, function(err, user) {
                if (!user) {
                    return res.redirect('back');
                }
                user.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10), null);
                user.email = req.body.email;
                user.save();
                req.flash('success', 'Information successfully updated!');
                res.redirect('back');
            });
        }
    });
    router.get('/profile', isAuthenticated, function(req, res) {
        res.render('profile', {
            user: req.user,
            error: req.flash('error'),
            success: req.flash('success')
        });
    });
    /* Handle Logout */
    router.get('/signout', function(req, res) {
        req.logout();
        res.redirect('/');
    });

    router.get('/users', isAdmin, function(req, res) {
        var page = (req.query.page === undefined) ? 1 : parseInt(req.query.page);
        var size = (req.query.size === undefined) ? 2 : parseInt(req.query.size);
        var skip = page > 0 ? ((page - 1) * size) : 0;
        var totalRecords = 0;
        User.count(function(err, result) {
            totalRecords = result;
        });
        if (req.query.result === undefined) {
            User.find(null, null, {
                    skip: skip,
                    limit: size
                },
                function(err, result) {
                    res.render('users', {
                        user: req.user,
                        users: result,
                        size: size,
                        totalRecords: totalRecords
                    });
                });
        } else {
            User.find(null, null, {
                    skip: skip,
                    limit: size
                },
                function(err, result) {
					var json = JSON.stringify({
						user: req.user,
						users: result,
						size: size,
                        totalRecords: totalRecords
					});
                    res.send(json);
                });
        }
    });

    router.post('/users', isAdmin, function(req, res) {
        var ObjectId = require('mongoose').Types.ObjectId;
        var id = req.body.user_id;
        console.log(id);
        var uname = req.body.uname_input;
        var fname = req.body.fname_input;
        var lname = req.body.lname_input;
        var email = req.body.email_input;
        var dmac = req.body.dmac_input;
        var isAdmin = req.body.isAdmin_input;

        console.log(req.body.dmac_input);
        console.log(req.body.lname_input);
        console.log(req.body.fname_input);
        console.log(req.body.email_input);
        console.log(req.body.uname_input);
        console.log(req.body.isAdmin_input);

        var query = {
            _id: new ObjectId(req.body.user_id)
        };
        var update = {
            deviceMac: dmac,
            lastName: lname,
            firstName: fname,
            email: email,
            username: uname,
            isAdmin: isAdmin
        }
        User.findOne({
            username: uname
        }, function(err, user) {
            console.log(user);
            if (!user) {

                console.log("error");
            }
            user.deviceMac = dmac,
			user.lastName = lname,
			user.firstName = fname,
			user.email = email,
			user.username = uname,
			user.isAdmin = isAdmin
            user.save();
            res.redirect('/users');
        });
    });

	router.get('/delete/:id', isAdmin, function(req,res){
		var last_name = req.query.name;
		var json;
		User.findOne( {'_id' : req.params.id }, function(err, user){
			if (!user) {
                console.log("error");
            }
			if (user.lastName === last_name){
				user.remove();
				json = JSON.stringify({
					isRemoved: true
				});
			} else {
				json = JSON.stringify({
					isRemoved: false
				});
			}
			res.send(json);
		});

	});
    return router;
}
