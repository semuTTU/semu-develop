
var validator = require('validator');
var mongoose = require('mongoose');
var User = require('../models/user');
var clients = {};
var webClients = {};
var console = process.console;

module.exports = function(io) {

  io.on('connection', function(socket) {
    console.info("New socket connection: " + socket.id);
    socket.auth = false;
    socket.on('authenticate', function(data) {
        if (validator.isMACAddress(data)) {
          User.findOne({
              'deviceMac': data
            }, function(err, result) {
              if (err)
                console.error("Device with MAC:" + data + " not found.");
              else if(result){
                socket.auth = true;
                socket.pi = true;
                socket.username = result.username;
                clients[socket.username] = socket;
                console.info("Socket " + socket.id + " succesfully authented as " + result.username + " device");
              }else{
                  console.error("Device with MAC:" + data + " not found.");
              }
            }
          );
      }
    });
    if(socket.handshake.session.passport){
      var userid = socket.handshake.session.passport.user;
      User.findById(userid, function (err, result) {
        if (err) {
          console.error("User with id: " + userid + " not found.");
        } else if(result){
          socket.username = result['username'];
          webClients[socket.username] = socket;
          socket.auth = true;
          console.info("Socket " + socket.id + " succesfully authented as " + result.username);
        }
      });
    }

    setTimeout(function() {
      if (!socket.auth) {
        console.error("Disconnecting socket: ", socket.id + " did not authenticate in time.");
        socket.disconnect('unauthorized');
      }
    }, 1000);

    socket.on('getDevices', function() {
      console.info('Get devices request from: ' + socket.username);
      var device = clients[socket.username];
      if(device){
        device.emit('getDevices');
      }else{
        webClients[socket.username].emit('devicesReply', "");
        console.error("Cannot get devices for " + socket.username + ": device not connected");
      }
    });


    socket.on('devicesReply', function(data) {
      console.info('Got devices for: ' + socket.username);
      var webClient = webClients[socket.username];
      if(webClient){
        webClient.emit('devicesReply', data);
      }else{
        console.error('Cannot send response to user' + socket.username +  ' user not connected' );
      }
    });

    socket.on('disconnect', function() {
      if(socket.pi){
        delete clients[socket.username];
        console.info(socket.username + ": device disconnected");
      }else{
        delete webClients[socket.username];
        console.info(socket.username + ": client disconnected");
      }
    });
    socket.on('turnOff', function(id) {
      var username = webClients[socket.username];
      var device = clients[socket.username];
      console.info(socket.username + " shutting down: " + id);
      if (device){
        device.emit('turnOff', id);
      }else{
        console.error("Cannot send turnoff request " + socket.username + ": device not connected");
      }

    });
     socket.on('turnOn', function(data) {
       var username = webClients[socket.username];
       var device = clients[socket.username];
       console.log(socket.username + " turning on: " + data);
       if (device){
         device.emit('turnOn', data);
       }else{
         console.error("Cannot send turnon request " + socket.username + ": device not connected");
       }
    });
	
	socket.on('deleteUser', function(user_id, last_name) {
		var found_user;
		User.findOne( {'_id' : req.params.id }, function(err, user){
			console.log(user.lastName);
			found_user = user;
				
		});
		var user_last_name =  user.lastName;
		if (last_name === user_last_name){
			console.log("ok");
		}
		//User.findOne( {'_id' : req.params.id }).remove().exec();

    });
  });
}
