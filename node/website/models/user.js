
var mongoose = require('mongoose');
var userSchema = mongoose.Schema({
	id: String,
	username: {type: String, required: true, unique: true},
	password: {type: String, required: true},
	email: {type: String, required: true},
	firstName: {type: String, required: true},
	lastName: {type: String, required: true},
	deviceMac: String,
	isAdmin: { type: Boolean, default: false },
	resetPasswordToken: String,
	resetPasswordExpires: Date
},{
		collection: 'users'
});

module.exports = mongoose.model('User', userSchema);
