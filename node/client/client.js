
var io = require('socket.io-client');
var socketClient = io.connect('http://172.24.1.100:3000', { //server ip here
    reconnect: true
});
var mac = require('getmac');
request = require('request-json');
var client = request.createClient('http://localhost:8083/');
client.setBasicAuth('admin', 'raspberry');


socketClient.on('connect', function(socket) {
    console.log('Connected!');
    sendMac();
});

function sendMac() {
    mac.getMac(function(err, macAddress) {
        if (err) throw err
        socketClient.emit('authenticate', macAddress);
    })
}

socketClient.on('message', function(data) {
    console.log(data);
});
socketClient.on('getDevices', function(data) {
    console.log('seaded');
    client.get('ZWaveAPI/Run/devices', function(error, res, body) {
        socketClient.emit('devicesReply', body);
        return console.log(body);
    });
});
socketClient.on('turnOff', function(data) {
	console.log('Turning device ' + data + ' off');
    client.get('ZWaveAPI/Run/devices[' + data + '].instances[0].Basic.Set(0)', function(err, res, body) {
        return console.log(body);
    });

});
socketClient.on('turnOn', function(data) {
	console.log('Turning device ' + data + ' on');
    client.get('ZWaveAPI/Run/devices[' + data + '].instances[0].Basic.Set(255)', function(err, res, body) {
        return console.log(body);
    });
});

var pythonClient = require('socket.io')(8999);

pythonClient.on('connection', function (socket) {
	
  socket.on('getDevicesPython', function (data) {
	client.get('ZWaveAPI/Run/devices', function(error, res, body) {
		var a = body,
			i;
		var keys = Object.keys(a);
		var response = [];
		for (i = 0; i < keys.length; i++) {
			var device = a[keys[i]]['data']['givenName']['value'];
			var status = a[keys[i]]['instances']['0']['commandClasses']['32']['data']['level']['value'];
			var id = a[keys[i]]['id'];
			if (device != null && status != null) {
				if (status == '255') {
					response.push([device, id]);
				}
			}
		}
		response = JSON.stringify(response);
        socket.emit('devicesReplyPython', response);
        return console.log(response);
    });
  });
  
  socket.on('turnOffPython', function (deviceId) {
    console.log('Turning device ' + deviceId + ' off');
    client.get('ZWaveAPI/Run/devices[' + deviceId + '].instances[0].Basic.Set(0)', function(err, res, body) {
        return console.log(body);
    });
  });
  
});